﻿namespace WarehouseImporter.Interfaces
{
    interface ICreator
    {
        void CreateFromText(string[] text);
    }
}
