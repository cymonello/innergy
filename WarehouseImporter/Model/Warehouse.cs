﻿using System.Collections.Generic;
using System.Linq;

namespace WarehouseImporter.Model
{
    public class Warehouse
    {
        public string Name { get; set; }
        public Dictionary<Material, int> Materials { get; set; }

        public Warehouse(string name)
        {
            Name = name;
            Materials = new Dictionary<Material, int>();
        }

        public void AddMaterialToDictionary(Material material, int amount)
        {
            var materialToUpdate = Materials.FirstOrDefault(m => m.Key.Id == material.Id);
            if (materialToUpdate.Key != null)
            {
                Materials[material] += amount;
            }
            else
            {
                Materials.Add(material, amount);
            }
        }
    }
}
