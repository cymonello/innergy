﻿namespace WarehouseImporter.Model
{
    public class Material
    {
        public string Id { get; set; }
        public string Name { get; set; }

        public Material(string id, string name)
        {
            Id = id;
            Name = name;
        }
    }
}
