﻿using System;
using WarehouseImporter.DataCollections;
using WarehouseImporter.IO;

namespace WarehouseImporter
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Please specify next materials (line by line) Lines started with # will be skipped. Write 'stop' for the end of input");
            var dataCollection = new DataCollection();
            var parser = new Parser(dataCollection);
            var presenter = new Presenter(dataCollection);

            while (true)
            {
                var inputTextLine = Console.ReadLine();
                if (inputTextLine == "stop")
                {
                    break;
                }

                if (!String.IsNullOrEmpty(inputTextLine) && !inputTextLine.StartsWith("#"))
                {
                    var response = parser.ParseTextLine(inputTextLine);
                    if (!String.IsNullOrEmpty(response))
                    {
                        Console.WriteLine(response);
                    }
                }
            }
            Console.WriteLine("\n\n");
            Console.Write(presenter.GenerateOutputString());
            Console.ReadKey();
        }
    }
}
