﻿using System.Text.RegularExpressions;
using WarehouseImporter.DataCollections;

namespace WarehouseImporter.IO
{
    public class Parser
    {
        private DataCollection _dataCollection;
        public Parser(DataCollection dataCollection)
        {
            _dataCollection = dataCollection;
        }

        public string ParseTextLine(string textLine)
        {
            var inputDataTable = Regex.Split(textLine, ";");
            if (inputDataTable.Length != 3)
            {
                return "Wrong data format!";
            }
            var materialCreator = new MaterialCreator.MaterialCreator(_dataCollection);
            materialCreator.CreateFromText(inputDataTable);
            return null;
        }
    }
}
