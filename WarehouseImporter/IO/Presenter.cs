﻿using System.Collections.Generic;
using System.Linq;
using WarehouseImporter.DataCollections;
using WarehouseImporter.Model;

namespace WarehouseImporter.IO
{
    public class Presenter
    {
        private DataCollection _dataCollection;

        public Presenter(DataCollection dataCollection)
        {
            _dataCollection = dataCollection;
        }

        public string GenerateOutputString()
        {
            var outputString = "";
            foreach (var warehouse in _dataCollection.Warehouses.OrderByDescending(w => w.Materials.Values.Sum()).ThenByDescending(w => w.Name))
            {
                outputString += AddWarehouse(warehouse);
            }

            return outputString;
        }

        private string AddWarehouse(Warehouse warehouse)
        {
            var totalMaterialsAmount = warehouse.Materials.Values.Sum();
            var outputString = $"[{warehouse.Name}] (total {totalMaterialsAmount})\n";
            foreach (var warehouseMaterial in warehouse.Materials.OrderBy(m => m.Key.Id))
            {
                outputString += AddMaterial(warehouseMaterial);
            }

            return outputString + "\n";
        }

        private string AddMaterial(KeyValuePair<Material, int> warehouseMaterial)
        {
            return $"[{warehouseMaterial.Key.Id}]: {warehouseMaterial.Value}\n";
        }
    }
}
