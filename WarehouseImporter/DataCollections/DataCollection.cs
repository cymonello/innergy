﻿using System.Collections.Generic;
using WarehouseImporter.Interfaces;
using WarehouseImporter.Model;

namespace WarehouseImporter.DataCollections
{
    public class DataCollection : IDataCollection
    {
        public List<Material> Materials { get; set; }
        public List<Warehouse> Warehouses { get; set; }

        public DataCollection()
        {
            Materials = new List<Material>();
            Warehouses = new List<Warehouse>();
        }
    }
}
