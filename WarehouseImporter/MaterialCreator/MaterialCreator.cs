﻿using System;
using System.Linq;
using System.Text.RegularExpressions;
using WarehouseImporter.DataCollections;
using WarehouseImporter.Interfaces;
using WarehouseImporter.Model;

namespace WarehouseImporter.MaterialCreator
{
    public class MaterialCreator : ICreator
    {
        private readonly DataCollection _dataCollection;
        public MaterialCreator(DataCollection dataCollection)
        {
            _dataCollection = dataCollection;
        }
        public void CreateFromText(string[] inputData)
        {
            var newMaterial = CreateMaterial(inputData[0], inputData[1]);
            var warehouses = Regex.Split(inputData[2], "\\|");
            foreach (var warehouse in warehouses)
            {
                var warehouseData = Regex.Split(warehouse, ",");
                var newWarehouse = CreateWarehouse(warehouseData[0]);
                var amount = Convert.ToInt32(warehouseData[1]);
                newWarehouse.AddMaterialToDictionary(newMaterial, amount);
            }
        }

        private Material CreateMaterial(string name, string id)
        {
            var existingMaterial = _dataCollection.Materials.FirstOrDefault(m => m.Id == id);
            if (existingMaterial != null)
            {
                return existingMaterial;
            }

            var newMaterial = new Material(id, name);
            _dataCollection.Materials.Add(newMaterial);
            return newMaterial;
        }

        private Warehouse CreateWarehouse(string name)
        {
            var existingWarehouse = _dataCollection.Warehouses.FirstOrDefault(w => w.Name == name);
            if (existingWarehouse != null)
            {
                return existingWarehouse;
            }

            var newWarehouse = new Warehouse(name);
            _dataCollection.Warehouses.Add(newWarehouse);
            return newWarehouse;
        }
    }
}
