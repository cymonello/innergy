﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using WarehouseImporter.DataCollections;
using WarehouseImporter.MaterialCreator;
using WarehouseImporter.Model;

namespace WarehouseImporterTests
{
    [TestClass]
    public class CreatorTests
    {
        [TestMethod]
        public void Creator_CreateMaterial_ExistingMaterialAndExisitngWarehouse()
        {
            // Arrange
            string data = "Yankee Hardware 110 Deg. Hinge;COM-123908;WH-A,10|WH-B,11";
            string[] dividedText = Regex.Split(data, ";");
            string name = dividedText[0];
            string id = dividedText[1];
            Material existingMaterial = new Material(id, name);
            Warehouse existingWarehouse = new Warehouse("WH-A");

            DataCollection dataCollection = new DataCollection();
            dataCollection.Materials.Add(existingMaterial);
            dataCollection.Warehouses.Add(existingWarehouse);
            MaterialCreator materialCreator = new MaterialCreator(dataCollection);

            // Act
            materialCreator.CreateFromText(dividedText);

            // Assert
            var sameMaterialsCount = dataCollection.Materials.Count(m => m.Id == id);
            Assert.AreEqual(sameMaterialsCount, 1);
        }

        [TestMethod]
        public void Creator_CreateMaterial_NotExistingMaterialAndNotExisitngWarehouse()
        {
            // Arrange
            string data = "Yankee Hardware 110 Deg. Hinge;COM-123908;WH-A,10|WH-B,11";
            string[] dividedText = Regex.Split(data, ";");
            string name = dividedText[0];
            string id = dividedText[1];
            Material expectedMaterial = new Material(id, name);
            Warehouse expectedWarehouse = new Warehouse("WH-A");

            DataCollection dataCollection = new DataCollection();
            MaterialCreator materialCreator = new MaterialCreator(dataCollection);

            // Act
            materialCreator.CreateFromText(dividedText);

            // Assert
            var materialFromCollectionCount = dataCollection.Materials.Count(m => m.Id == expectedMaterial.Id);
            var warehouseFromCollectionCount = dataCollection.Warehouses.Count(w => w.Name == expectedWarehouse.Name);
            Assert.AreEqual(materialFromCollectionCount, 1);
            Assert.AreEqual(warehouseFromCollectionCount, 1);
        }

    }
}
