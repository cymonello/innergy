﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using WarehouseImporter.DataCollections;
using WarehouseImporter.IO;

namespace WarehouseImporterTests
{
    [TestClass]
    public class ParserTests
    {
        [TestMethod]
        public void Parser_ParseTextLine_Valid()
        {
            // Arrange
            DataCollection dataCollection = new DataCollection();
            Parser parser = new Parser(dataCollection);
            string textLine = "Generic Wire Pull;COM-123906c;WH-A,10|WH-B,6|WH-C,2";
            string expectedOutput = null;

            // Act
            var result = parser.ParseTextLine(textLine);

            // Assert
            Assert.AreEqual(expectedOutput, result);
        }

        [TestMethod]
        public void Parser_ParseTextLine_Invalid()
        {
            // Arrange
            DataCollection dataCollection = new DataCollection();
            Parser parser = new Parser(dataCollection);
            string textLine = "Generic Wire Pull;COM-123906c";
            string expectedOutput = "Wrong data format!";

            // Act
            var result = parser.ParseTextLine(textLine);

            // Assert
            Assert.AreEqual(expectedOutput, result);

        }
    }
}
